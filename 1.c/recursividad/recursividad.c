#include <stdio.h>
#include <stdlib.h>

long double arr[10000] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

int r_p(int i)
{
    if(i <= 10000) {
        getchar();
        return 0;
    }
    else {
        return arr[i] + r_p(i+1);
    }
}

int r_c(int i, int a)
{
  if(i <= 10000) {
        // getchar();
        return 0;
    }
    else {
        return r_c(i+1, a+arr[i]);
    }
}

long long fibonacci_iterativo(long long n)
{
    long long i = 0;
    long long j = 1;
    for (long long k = n; k >= 0; k--)
    {
        long long t;
        t = i + j;
        i = j;
        j = t;
    }
    return j;
}

long long fibonacci_recursivo(long long n)
{
    long long resultado = 0;
    if (n == 0)
    {
        resultado = 0;
    }
    else if (n == 1)
    {
        resultado = 1;
    }
    else
    {
        resultado = fibonacci_recursivo(n - 1) + fibonacci_recursivo(n - 2);
    }
    return resultado;
}

long long factorial_iterativo(int n)
{
    long long resultado = 1;
    for (long long i = n; i > 0; i--)
    {
        resultado *= i;
    }
    return resultado;
}

long long factorial_recursivo(long long n) //  <=================
{                                          // ||
    long long resultado = -1;              // ||
    if (n == 1)                            // ||
    {                                      // ||
        // caso base                            // ||
        resultado = 1; // ||
    }                  // ||
    else               // ||
    {                  // ||
        // caso recursivo                       // ||
        resultado = factorial_recursivo(n - 1) * n; // ========
    }
    return resultado;
}

int main(int argc, char **argv)
{
    // printf("%lld! = %lld\n", atoi(argv[1]), factorial_recursivo(atoi(argv[1])));
    // printf("%lld! = %lld\n", atoi(argv[1]), factorial_iterativo(atoi(argv[1])));

    // printf("fib(%lld) = %lld\n", atoi(argv[1]), fibonacci_recursivo(atoll(argv[1])));
    // printf("fib(%lld) = %lld\n", atoi(argv[1]), fibonacci_iterativo(atoll(argv[1])));


    printf("%ld\n", r_c(0, 0));






    return 0;
}