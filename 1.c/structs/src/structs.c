#include "structs.h"

double imprimir_tren_x(char c, long l , int i0, int i1) {
    printf(
        "Tren:\n\t%c\n\t%ld\n\t%d\n\t%d\n",
        c, l, i0, i1
    );

    return 1.0/2.0;
}

double f(char c, long l, int i0, int i1){

}


void hacer_structs() {

    // tren
    int ruedas;
    int vagones;
    float velocidad_maxima;
    char tipo; /// V, E, D, M
    long pasajeros;
    int carga;
    char* tipo_de_carga;


    
    struct Fecha_fabricacion {
        unsigned int dia, mes, anyio;
    };
    

    
    struct Tren {
        struct Fecha_fabricacion fecha;
        int ruedas;
        int vagones;
        float velocidad_maxima;
        char tipo; /// V, E, D, M
        long pasajeros;
        int carga;
        char* tipo_de_carga;
        double (*imprimir_tren)(char, long, int, int);
    };


    struct Fecha_fabricacion _f0;
    _f0.dia = 1;
    _f0.anyio = 3000;
    _f0.mes = 2;

    struct Fecha_fabricacion _f1;
    _f1.dia = 32;
    _f1.anyio = 1397;
    _f1.mes = 2;

    struct Tren _t0;
    _t0.pasajeros  = 300;
    _t0.tipo = 'M';
    _t0.fecha = _f0;
    // ...

    struct Tren _t1;
    _t0.carga  = 300;
    _t0.tipo = 'D';
    _t0.fecha = _f1;
    // ...

    printf(
        "Tren:\n\t%d/%d/%d\n\t%d\n\t%d\n\t%f\n\t%c\n\t%ld\n\t%d\n\t%s\n",
        _t0.fecha.anyio, _t0.fecha.mes, _t0.fecha.dia,
        _t0.ruedas, 
        _t0.vagones, 
        _t0.velocidad_maxima, 
        _t0.tipo, 
        _t0.pasajeros, 
        _t0.carga, _t0.tipo_de_carga 
    );

    struct Tren* _t2_ptr;
    _t2_ptr = (struct Tren*) malloc (1*sizeof(struct Tren));

    _t2_ptr->fecha = _f0;
    _t2_ptr->carga = 50;
    _t2_ptr->pasajeros = 250;
    _t2_ptr->velocidad_maxima = 420;
    _t2_ptr->tipo = 'L';

    _t2_ptr->imprimir_tren = &imprimir_tren_x;

    _t2_ptr->imprimir_tren(_t2_ptr->tipo, _t2_ptr->pasajeros,
        _t2_ptr->carga, _t2_ptr->pasajeros);

    _t0.imprimir_tren = &f;


    // printf(
    //     "Tren:\n\t%d/%d/%d\n\t%d\n\t%d\n\t%f\n\t%c\n\t%ld\n\t%d\n\t%s\n",
    //     _t2_ptr->fecha.anyio, _t2_ptr->fecha.mes, _t2_ptr->fecha.dia,
    //     _t2_ptr->ruedas, 
    //     _t2_ptr->vagones, 
    //     _t2_ptr->velocidad_maxima, 
    //     _t2_ptr->tipo, 
    //     _t2_ptr->pasajeros, 
    //     _t2_ptr->carga, _t2_ptr->tipo_de_carga 
    // );

    struct Fecha_fabricacion _x, _y, _z;
    struct Fecha_fabricacion arreglo_fechas [3] = { _x, _y, _z };

    free(_t2_ptr);

    return;

}
















