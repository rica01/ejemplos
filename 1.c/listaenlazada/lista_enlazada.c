#include <stdlib.h>
#include <stdio.h>

#define FIN 0

typedef struct _pos
{
    char c;
    struct _pos *siguiente;
} pos;

void imprimir_arreglo(char *arr, int N)
{
    for (int i = 0; i < N; i++)
        printf("%c\n", arr[i]);
    return;
}

void imprimir_lista_rec(pos *p)
{
    if (p == FIN)
    {
        return;
    }
    else
    {
        printf("%c\n", p->c);
        imprimir_lista_rec(p->siguiente);
    }
}

void imprimir_lista(pos *primero)
{

    pos *cursor = primero;
    while (cursor != FIN)
    {
        printf("%c\n", cursor->c);
        cursor = cursor->siguiente;
    }
    return;
}

int main()
{
    char c[3];
    c[0] = 'o';
    c[1] = 'l';
    c[2] = 'a';

    imprimir_arreglo(c, 3);

    ///////

    pos *la_o;
    pos *la_l;
    pos *la_a;

    la_o = malloc(sizeof(pos));
    la_l = malloc(sizeof(pos));
    la_a = malloc(sizeof(pos));

    la_o->c = 'O';
    la_l->c = 'L';
    la_a->c = 'A';

    la_o->siguiente = la_l;
    la_l->siguiente = la_a;
    la_a->siguiente = FIN; // 0x0 // 0 // NULL

    imprimir_lista(la_o);

    imprimir_lista_rec(la_o);
    ///////


    
    return 0;
}