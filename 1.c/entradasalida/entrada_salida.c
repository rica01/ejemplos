#include <stdlib.h>
#include <stdio.h>

// argc: la cantidad de argumentos enviados por la CLI
// argv: los valores de los argument os enviados por la CLI
int main(int argc, char** argv) {
    printf("cuantos argumentos? %d\n\n", argc);
    for(int i = 0; i < argc; i++) {
        printf("%d: %s\n", i, argv[i]);
    }

    // scanf, printf
    // fscanf, fprintf

    // m / h*h
    float m;
    float a;
    char n[128];

    printf("Digite su nombre: ");
    scanf("%s", n);
    printf("\n");

    printf("Digite su masa: ");
    scanf("%f", &m);
    printf("\n");

    printf("Digite su altura: ");
    scanf("%f", &a);
    printf("\n");

    float bmi = m / (a*a);

    printf("%s, su indice de masa coooorporal es: %f\n", n, bmi);

    return 0;
}