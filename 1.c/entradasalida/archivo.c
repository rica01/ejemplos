#include <stdlib.h>
#include <stdio.h>

// argc: la cantidad de argumentos enviados por la CLI
// argv: los valores de los argument os enviados por la CLI
int main(int argc, char** argv) {
    printf("cuantos argumentos? %d\n\n", argc);
    for(int i = 0; i < argc; i++) {
        printf("%d: %s\n", i, argv[i]);
    }

    float m;
    float a;
    char n[128];
    FILE* f_in;
    FILE* f_out;

    f_in = fopen(argv[1], "r");
    fscanf(f_in, "%s\n", n);
    fscanf(f_in, "%f\n", &m);
    fscanf(f_in, "%f\n", &a);
    fclose(f_in);

    float bmi = m / (a*a);

    f_out = fopen(argv[2], "w");
    fprintf(f_out, "%s, su indice de masa coooorporal es: %f\n", n, bmi);
    fclose(f_out);

    return 0;
}




