#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *strrpl(char *origen, char *destino, char *contenido)
{
    // printf("\n%s\n", "----------");

    char *r = (char *)malloc(1000000 * sizeof(char));
    char *si_lo_encontro = strstr(contenido, origen);

    long int salto = (si_lo_encontro - contenido);

    //copiar lo esta antes
    strncat(r, contenido, salto);

    // copiar el reemplazo
    strcat(r, destino);

    //copiar lo q esta despues
    strcat(r, contenido + salto + strlen(origen));

    // printf("\n%s\n(%ld)\n", r, strlen(r));

    // printf("\n%s\n", "----------");
    return r;
}

int main(int argc, char **argv)
{
    printf("Clonador de gatos\n");
    printf("\tClonando a %s en %s\n", argv[1], argv[2]);

    FILE *el_original = fopen(argv[1], "r");

    char linea[512] = {'\0'};
    char *contenido = (char *)malloc(1000000 * sizeof(char));
    
    // while (fgets(linea, 512, el_original))
    // {
    //     printf("\t\t%s", linea);
    //     fputs(linea, el_clon);
    // }

    while (fgets(linea, 512, el_original))
    {
        strcat(contenido, linea);
    }

    char *r = strrpl("marco", "marclon", contenido);
    FILE *el_clon = fopen(argv[2], "w");
    fputs(r, el_clon);

    // printf("%s\n", contenido);

    fclose(el_original);
    fclose(el_clon);
    free(contenido);
    free(r);

    return 0;
}
