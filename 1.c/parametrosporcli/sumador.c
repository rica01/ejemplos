#include <stdlib.h>
#include <stdio.h>

// argc: la cantidad de argumentos enviados por la CLI
// argv: los valores de los argument os enviados por la CLI
int main(int argc, char** argv) {
    printf("cuantos argumentos? %d\n\n", argc);
    for(int i = 0; i < argc; i++) {
        printf("%d: %s\n", i, argv[i]);
    }

    int a;
    int b;

    a = atoi( argv[1] );
    b = atoi( argv[2] );

    int c = a+b;

    printf("%d + %d = %d\n\n", a, b ,c);

    return 0;
}