#include <stdlib.h>
#include <stdio.h>

#include <gtk/gtk.h>


void cerrar(GtkWidget* w, gpointer d){
   gtk_main_quit();
}


void apretar(GtkWidget* w, gpointer d){
   printf("he sido apretado!\n");
}


void estripar(GtkWidget* w, gpointer d){
   printf("he sido estripado!\n");
}


int main(int c, char ** v) {

   gtk_init(&c, &v);

   GtkWidget* win;
   GtkWidget* btn1;
   GtkWidget* btn2;

   win = gtk_window_new( GTK_WINDOW_TOPLEVEL );
   g_signal_connect(win, "destroy", G_CALLBACK(cerrar), NULL);
   gtk_container_set_border_width(GTK_CONTAINER(win), 20);

   btn1 = gtk_button_new_with_label("apretame!");
   g_signal_connect(btn1, "clicked", G_CALLBACK(apretar), "button");

   btn2 = gtk_button_new_with_label("estripame!");
   g_signal_connect(btn2, "clicked", G_CALLBACK(estripar), "button");

   gtk_container_add( GTK_CONTAINER(win), btn1 );
   gtk_container_add( GTK_CONTAINER(win), btn2 );

   gtk_widget_show_all(win);

   gtk_main();



   return 0;
}






