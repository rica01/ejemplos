#ifndef FUNCIONES_H
#define FUNCIONES_H


#include <stdio.h>
#include "datos.h"


double calcular_peso_en(double masa, double g);

void suma_escalar_vector_1(short a, short _arr[ ], int n); // direccion
void suma_escalar_vector_2(short a, short _arr[4]); // arreglo de 4
void suma_escalar_vector_3(short a, short * _arr, int n); // puntero

short* shakira(int f);

void imprimir(struct AC ac);
void imprimir_ptr(aire_acondicionado * ac);

void imprimir_bolsa(bolsa* b);

#endif