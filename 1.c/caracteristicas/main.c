#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <stdbool.h>

#define BYTE char
#define PI 3.14

#include "./funciones.h"
#include "datos.h"

// void main() {}
// main() {}
// int main() {}
// void main(int, char**) {}
// main() {int c, char**) {}

int main(int argc, char **argv)
{

	////////////////////////////////////
	// numeros enteros

	BYTE _c0; // declaracion
	_c0 = 0;  // asignacion

	BYTE _c1 = 1; // 1 byte => 8 bits (2^8 -1 => 256): [-127, 128]
	_c1 = 250;

	printf("_c0 = %d\n", _c0);
	printf("_c1 = %d\n", _c1);

	unsigned BYTE _c2; // 1 byte: [0, 255]
	_c2 = 145;
	printf("_c2 = %d\n", _c2);

	////////////////////////

	short _s0; // 2 byte; [-32k , 32k]

	long l = sizeof(_s0);
	printf("la l: %ld\n", l);

	unsigned short _s1 = 42; // 2 byte; [0 ,65k]

	l = sizeof(_s1);
	printf("la l: %ld\n", l);

	////////////////////////////////

	int _i0; // 4 bytes; [-2G, 2G]
	_i0 = 1231231212;

	unsigned int _i1; // 4 bytes; [0, 4G]
	_i1 = 1231231212;

	printf("_i1 (%Ld): %d\n", sizeof(_i1), _i1);

	///////////////////////////////////

	long _l0 = -346754723465736855;	// 8 bytes; []
	unsigned long _l1 = 5646745684678; // 8 bytes; []

	printf("_l1 (%ld): %ld\n", sizeof(_l1), _l1);

	////////////////////////////////////////

	long long _ll0 = 22345263456356345;
	printf("_ll0 (%lld): %lld\n", sizeof(_ll0), _ll0);

	////////////////////////////////////////
	// numeros reales

	float _f0 = 52345.23452;
	double _d0 = 56345.3563456;
	long double _ld0 = 263456385678.787684568;

	//////////////////////////

	printf("maximo numero en un int %lld\n", INT_MAX);
	printf("maximo numero en un double %lf\n", DBL_MAX);

	///////////////////////////
	///////////////////////////
	///////////////////////////
	// texto

	char _t0 = 'F';			  // texto, 1 byte.
	printf("_t0: %c\n", _t0); // como caracter
	printf("_t0: %d\n", _t0); // como numero

	char _t1 = 70; // texto, 1 byte.
	printf("_t1: %c\n", _t1);
	printf("_t1: %d\n", _t1);

	char _t2 = '3';

	///////////////////////

	char _n0 = 'A';
	char _n1 = 'n';
	char _n2 = 'a';

	char _nA0[3];
	printf("%d ", _nA0[0]);
	printf("%d ", _nA0[1]);
	printf("%d\n", _nA0[2]);

	_nA0[0] = 'A';
	_nA0[1] = 78;
	_nA0[2] = 'A';

	printf("%c ", _nA0[0]);
	printf("%c ", _nA0[1]);
	printf("%c\n", _nA0[2]);

	char _nA1[3] = {'A', 'n', 'a'};

	printf("%c ", _nA1[0]);
	printf("%c ", _nA1[1]);
	printf("%c\n", _nA1[2]);

	char _nA2[1000000] = "Ana";

	printf("%s\n", _nA2);
	printf("%d\n", _nA2[3]); // caracter de fin de hilera '\0'

	//
	//
	//

	bool _b0 = true;
	printf("_b0: %d\n", _b0);

	/////////////////////////////////////////
	/////////////////////////////////////////
	/////////////////////////////////////////
	// arreglos
	// punteros

	// TIPO id[N];
	short _arr[4] = {9, 67, 240, 29}; // short -> 2 bytes

	printf("soy un nueve (%d)\n", _arr[0]);
	short *ptr_arr = &_arr;

	printf("%p\n%p\n%p\n\n", &_arr, _arr, ptr_arr);

	ptr_arr = &_arr[2];

	printf("%p\n%p\n\n", &_arr[2], ptr_arr);

	////

	printf("%d - %d\n\n", _arr[2], *(ptr_arr + 0));

	// ? 1, 2, 8, -1???
	printf("el sizeof: %ld\n\n", sizeof(_arr));	// 8 (alfonso)
	printf("el sizeof: %ld\n\n", sizeof(_arr[0])); // 2 (gabriel)
	printf("el sizeof: %ld\n\n", sizeof(&_arr));   // si son 8!(ana)
	printf("el sizeof: %ld\n\n", sizeof(*_arr));   // 2 (andres)
	printf("el sizeof: %ld\n\n", sizeof(ptr_arr)); // 8 (isaac)

	for (int i = 0; i < 4; i++)
	{
		printf("%d\n", _arr[i]);
	}
	printf("\n");

	for (int i = 0; i < 4; i++)
	{
		printf("%d\n", *(_arr + i));
	}
	printf("\n");

	// V=(1,2), a=3 => a+V = (4,5)

	////////////////////////
	// estructuras (uniones, enumeracion)

	// ver datos.h

	struct bolsa_de_MMA mi_bolsa;

	mi_bolsa.peso = 12.3;
	mi_bolsa.talla = 12.3;
	mi_bolsa.marca = "MarKa";
	mi_bolsa.serie = 12;

	bolsa otro_bolsa_mia;
	otro_bolsa_mia.marca = "neverlast";
	/* ... */

	struct AC friser;

	friser.potencia = 2.6;
	friser.control_remoto = true;
	friser.marca = "La";
	friser.modelo = "marca";
	friser.calorias = 80;

	printf("Aire acondionado original (%p):\n", &friser);

	imprimidora dora;
	dora.imprimir_ac = &imprimir_ptr;
	dora.imprimir_bolsa = &imprimir_bolsa;

	dora.imprimir_ac(&friser);
	dora.imprimir_bolsa(&otro_bolsa_mia);


	// funciones

	suma_escalar_vector_1(1, _arr, 4);
	suma_escalar_vector_2(2, _arr);
	suma_escalar_vector_3(3, &_arr, 4);

	// puntero a funcion
	// TIPODERETORNO (*ID)(TIPO ARG1, TIPO ARG2 ...)
	double (*ptr_funcion)(double m, double g);
	ptr_funcion = &calcular_peso_en;

	printf("%lf\n", ptr_funcion(45.4, 9.1));
	printf("%lf\n", calcular_peso_en(45.4, 9.1));

	short *shak = shakira(11);
	printf("shak: %p\n", shak);
	printf("shak: %d\n", shak[0]);

	double p;
	p = calcular_peso_en(4.0, 3.711);
	printf("El peso de Cangreja en Marte es: %lf\n", p);

	//////////////////////////////
	printf("\n\n\n\n\n\n\n\n\n");
	return 0;
}
