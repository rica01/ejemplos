#include "./funciones.h"

/*
TIPO_DE_RETORNO identificador(TIPO arg1, TIPO arg2, .... )
{

    return TIPO_DE_RETORNO;
}
*/
/**
* @brief funcion para calcular el peso
*
* @param masa valor real.
* @param g valor real.
* @return double el peso.
**/
double calcular_peso_en(double masa, double g)
{
    double peso = masa * g;
    return peso;
}

void suma_escalar_vector_1(short a, short _arr[], int n)
{
    printf("el 1\n");
    for (int i = 0; i < n; i += 1)
    {
        _arr[i] = _arr[i] + a;
    }

    for (int i = 0; i < n; i += 1)
        printf("%d\n", _arr[i]);

    printf("\n");
    return;
}

void suma_escalar_vector_2(short a, short _arr[4])
{
    printf("el 2\n");
    for (int i = 0; i < 4; i += 1)
    {
        _arr[i] = _arr[i] + a;
    }

    for (int i = 0; i < 4; i += 1)
        printf("%d\n", _arr[i]);

    printf("\n");
    return;
}

void suma_escalar_vector_3(short a, short *_arr, int n)
{
    printf("el 3\n");
    for (int i = 0; i < n; i += 1)
    {
        // _arr[i] = _arr[i] + a;
        *(_arr + i) = *(_arr + i) + a;
    }

    for (int i = 0; i < n; i += 1)
        printf("%d\n", *(_arr + i));

    printf("\n");
    return;
}


short* shakira(int f) 
{
    short arreglito[f];
    printf("shakira: %p\n", arreglito);
    return arreglito;

}


void imprimir_ptr(struct AC * ptr_ac){

    printf("Aire acondionado (%p):\n", ptr_ac);
    printf("\tpotencia: %lf\n", ptr_ac->potencia);
    printf("\tcontrol_remoto: %d\n", ptr_ac->control_remoto);
    printf("\tmarca: %s\n", ptr_ac->marca);
    printf("\tmodelo: %s\n", ptr_ac->modelo);
    printf("\tcalorias: %d\n", ptr_ac->calorias);
    
    return;
}

void imprimir(struct AC ac){
    printf("Aire acondionado (%p):\n", &ac);
    printf("\tpotencia: %lf\n", ac.potencia);
    printf("\tcontrol_remoto: %d\n", ac.control_remoto);
    printf("\tmarca: %s\n", ac.marca);
    printf("\tmodelo: %s\n", ac.modelo);
    printf("\tcalorias: %d\n", ac.calorias);
    
    return;
}



void imprimir_bolsa(bolsa* b){
    printf("%p\n", b);
    return;
}