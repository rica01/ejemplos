#include <SFML/Audio.h>
#include <SFML/Graphics.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{

    sfVideoMode mode = {1000, 600, 32};
    sfRenderWindow *window = sfRenderWindow_create(mode, "bichito", sfResize | sfClose, NULL);

    sfFont *style = sfFont_createFromFile("res/pd.ttf");
    sfText *text = sfText_create();
    sfText_setFont(text, style);
    sfText_setString(text, "recoger las cajas de jugo de naranja porfavor");
    sfText_setCharacterSize(text, 60);

    sfEvent event;
    while (sfRenderWindow_isOpen(window))
    {
        while (sfRenderWindow_pollEvent(window, &event))
        {
            if (event.type == sfEvtClosed)
            {
                printf("cerrando ventana\n");
                sfRenderWindow_close(window);
            }
            if (event.key.code == sfKeyS)
            {
                printf("s\n");
                sfVector2f  v;
                v = sfText_getPosition(text);
                v.y=v.y+10;
                sfText_setPosition(text, v);
            }
            if (event.key.code == sfKeyA)
            {
                printf("a\n");
                sfVector2f  v;
                v = sfText_getPosition(text);
                v.x=v.x-10;
                sfText_setPosition(text, v);
            }
            if (event.key.code == sfKeyD)
            {
                printf("d\n");
                sfVector2f  v;
                v = sfText_getPosition(text);
                v.x=v.x+10;
                sfText_setPosition(text, v);
            }
            if (event.key.code == sfKeyW)
            {
                printf("w\n");
                sfVector2f  v;
                v = sfText_getPosition(text);
                v.y=v.y-10;
                sfText_setPosition(text, v);
            }
        }

        
        sfRenderWindow_clear(window, sfGreen);
        sfRenderWindow_drawText(window, text, NULL);
        sfRenderWindow_display(window);
    }

    return 0;
}
