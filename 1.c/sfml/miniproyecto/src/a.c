#include <SFML/Audio.h>
#include <SFML/Graphics.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
    sfVector2f v;



    sfVideoMode mode = {1000, 600, 32};
    sfRenderWindow* window = sfRenderWindow_create(mode, "bichito", sfResize | sfClose, NULL);
    sfTexture* _texture;

    sfSprite* bg = sfSprite_create();
    sfSprite* car = sfSprite_create();

    sfTexture* _t = sfTexture_createFromFile("res/bg.jpg", NULL);
    sfSprite_setTexture(bg, _t, sfTrue);
    float _x  = (float)sfTexture_getSize(_t).x;
    float _y  = (float)sfTexture_getSize(_t).y;

    v = (sfVector2f){
        sfRenderWindow_getSize(window).x/_x, sfRenderWindow_getSize(window).y/_y
    };
    sfSprite_setScale(bg, v);
    sfSprite_setTexture(car, sfTexture_createFromFile("res/giphy.gif", NULL), sfTrue);
    v = (sfVector2f){0.5, 0.5};
    sfSprite_scale(car, v);

    v = (sfVector2f){ sfRenderWindow_getSize(window).x/2, 0.7 * sfRenderWindow_getSize(window).y };
    sfSprite_setPosition(car, v);

    sfFont* style = sfFont_createFromFile("res/pd.ttf");
    sfText* text = sfText_create();
    sfText_setFont(text, style);
    sfText_setString(text, "recoger las cajas de jugo de naranja porfavor");
    sfText_setCharacterSize(text, 60);

    sfMusic* music = sfMusic_createFromFile("res/nice_music.ogg");
    sfSoundBuffer* sfx_buf = sfSoundBuffer_createFromFile("res/ws.ogg");
    sfSound* sfx = sfSound_create();
    sfSound_setBuffer(sfx, sfx_buf);

    sfEvent event;

    

    sfMusic_play(music);
    while( sfRenderWindow_isOpen(window) ){
        while( sfRenderWindow_pollEvent(window, &event) ){
            if (event.type == sfEvtClosed) {
                sfRenderWindow_close(window);
            }
            if (event.type == sfEvtKeyPressed) {
                if(event.key.code == sfKeyW){
                    printf("w\n");
                    sfSprite_setPosition(car, 
                        (sfVector2f){ sfSprite_getPosition(car).x ,sfSprite_getPosition(car).y-2 }
                    );
                }
                if(event.key.code == sfKeyA){
                    printf("a\n");
                    sfSprite_setPosition(car, 
                        (sfVector2f){ sfSprite_getPosition(car).x-2 ,sfSprite_getPosition(car).y }
                    );
                }
                if(event.key.code == sfKeyS){
                    printf("s\n");
                    sfSprite_setPosition(car, 
                        (sfVector2f){ sfSprite_getPosition(car).x ,sfSprite_getPosition(car).y+2 }
                    );
                }
                if(event.key.code == sfKeyD){
                    printf("d\n");
                    sfSprite_setPosition(car, 
                        (sfVector2f){ sfSprite_getPosition(car).x+2 ,sfSprite_getPosition(car).y }
                    );
                }

            }
            if (event.type == sfEvtMouseButtonPressed) {
                sfSprite_setPosition(car,
                    (sfVector2f) { rand()%sfRenderWindow_getSize(window).x, rand()%sfRenderWindow_getSize(window).y }
                );
            }



        }



        sfRenderWindow_clear(window, sfCyan);
        sfRenderWindow_drawSprite(window, bg, NULL);
        sfRenderWindow_drawSprite(window, car, NULL);
        sfRenderWindow_drawText(window, text, NULL);

        sfRenderWindow_display(window);
    }

    // destruir todo.












    return 0;
}










