/**
 * @file resumen.c
 * @author Ricardo
 * @brief Ejemplo de memoria dinamica
 * @version 1
 * @date 2019-05-20
 * 
 * @copyright Copyleft (l) 2019
 * 
 */
#include <stdlib.h>
#include <stdio.h>

// comentario de 1 linea
/*
comentario multilinea
*/

/**
 * @brief Es una descripcion breve de algo, en este caso de fun. La funcion fun recibe una cnatidad de bytes x y retorna un puntero de enteros de x campos.
 * 
 * @param x Cantidad de bytes del nuevo puntero.
 * @return int* Direccion del nuevo puntero de x campos.
 */
int* fun(int x){
    int* qwerty = (int*)malloc(x * sizeof(int));
    return qwerty;
}

/**
 * @brief Funcion de inicio del programa.
 * 
 * @param argc Cantidad de argumentos por CLI.
 * @param argv Valor de argumentos por CLI.
 * @return int Codigo de salida del programa.
 */ 
int main(int argc, char** argv)
{

    int cosa[5] = {10,11,12,13,14};

    int* cosa_ptr = fun(5);
    *(cosa_ptr+0) = 10;
    *(cosa_ptr+1) = 11;
    *(cosa_ptr+2) = 12;
    *(cosa_ptr+3) = 13;
    *(cosa_ptr+4) = 14;

    int i = 0;

    printf("%d\n", cosa[i]);
    printf("%d\n", cosa_ptr[i]);

    printf("%d\n", *(cosa+i));
    printf("%d\n", *(cosa_ptr+i));
    
    i = 3;

    printf("%d\n", cosa[i]);
    printf("%d\n", cosa_ptr[i]);

    printf("%d\n", *(cosa+i));
    printf("%d\n", *(cosa_ptr+i));
    
    free(cosa_ptr);


    double* experimento = (double*) malloc (5 * sizeof(double) );

    experimento[4] = 4.20;
    printf("%lf\n", experimento[4]); 

    

    double* resultado = realloc(experimento, 1000001*sizeof(double));
    printf("%ld\n%p\n%p\n", sizeof(double), experimento, resultado);
    if(resultado != 0x0) // si se pudo reasignar
    {
        free(experimento);
        experimento = resultado;
        experimento[1000000] = 666.0;
        printf("%lf\n", experimento[1000000]);
    }
    else //resultado == 0x0 (null) no se pudo reasignar
    {
        printf("salado!\n");
    }


    return 0;
}





