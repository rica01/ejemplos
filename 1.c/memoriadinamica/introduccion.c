#include <stdlib.h>
#include <stdio.h>
// #include <stdbool.h>

#define bool int
#define false 0

typedef struct _mouse
{
    int DPI;
    int botones;
    bool tipo;
} mouse;

int f()
{
    char c;
    int i;
    double d;
    char *ptr_c = &c;

    printf("%p\n", &c);
    printf("%p\n", &i);
    printf("%p\n", &d);
    printf("%p\n", &ptr_c); // puntero doble
    mouse raton;
    return 0;
}

unsigned short *g()
{
    float f;               // pila
    mouse speedy_gonzalez; // pila
    int i;                 // pila
    long l;                // pila

    unsigned short *ptr_s; // pila
    ptr_s = (unsigned short *)malloc(sizeof(unsigned short) * 1);
    *ptr_s = 4; // heap

    printf("%p\n", &f);
    printf("%p\n", &speedy_gonzalez);
    printf("%p\n", &i);
    printf("%p\n", &l);
    printf("ptr_s de g %p\n", &ptr_s);
    printf("%p\n", ptr_s);
    printf("%p\n", &(*ptr_s));

    return ptr_s;
}

mouse *h()
{
    mouse *jerry = (mouse *)malloc(1 * sizeof(mouse));
    jerry->DPI = 6932423;
    jerry->tipo = false;
    jerry->botones = 9;

    return jerry;
}

mouse *i(int N)
{

    mouse *molote;
    molote = (mouse *)malloc(sizeof(mouse) * N);

    printf("ptr_s del molote %p\n", &molote);
    printf("%p\n", molote);

    printf("sizeof mouse: %ld\n", sizeof(mouse));
    printf("%d\n", molote[2]);

    printf("%p\n", (molote + 2));
    printf("%d\n", (*(molote + 2)));

    return molote;
}

float** j()
{
    float A[2][2] = {-1.0, 0.0, 0.0, -1.0};

    printf("%p\n", &A);
    printf("%p\n", &A[0][0]);
    printf("%p\n", &A[0][1]);
    printf("%p\n", &A[1][0]);
    printf("%p\n", &A[1][1]);

    float **B = malloc(sizeof(float *) * 2); // columnas

    B[0] = malloc(sizeof(float) * 2); // 1 fila
    B[1] = malloc(sizeof(float) * 2); // 1 fila

    //   1.0   0.0
    //   0.0   1.0
    B[0][0] = 1.0;
    B[0][1] = 0.0;
    B[1][1] = 1.0;
    B[1][0] = 0.0;

    printf("%p\n", &B);
    printf("%p\n", &B[0][0]);
    printf("%p\n", &B[0][1]);
    printf("%p\n", &B[1][0]);
    printf("%p\n", &B[1][1]);

    return B;
}

void kk()
{
    float****** tensor6;
    // tensor6=(float******) malloc( sizeof(float*****) *4);
    /*...*/
    return;
}


int main()
{

    f();
    printf("-------------------\n");


    unsigned short *ptr_s = g();
    *ptr_s = 123;
    printf("-------------------\n");
    printf("ptr_s del main %p\n", &ptr_s);
    printf("%p\n", ptr_s);
    printf("%d\n", *ptr_s);
    free(ptr_s);


    mouse *ptr_jerry = h();
    printf("-------------------\n");
    printf("ptr_s del jerry %p\n", &ptr_jerry);
    printf("%p\n", ptr_jerry);
    printf("%d\n", ptr_jerry->DPI);

    printf("-------------------\n");

    mouse *ptr_molote = i(10);

    printf("-------------------\n");
    float** m = j();



    free(ptr_jerry);
    free(ptr_molote);

    free(m[0]);
    free(m[1]);
    free(m);



    return 0;
}

struct Mat 
{
    float** data;
    int fil;
    int col;
};