#include <stdlib.h>
#include <stdio.h>

void imprimirPlanetas(char* p, int i){
    for(int j = 0; j < i; j++)
        printf("%c\n",p[j]);
    printf("---------------------\n");
    return;
}



char* estirar(char* viejo, int t_viejo, int t_nuevo){
    char* nuevo = (char*) malloc( t_nuevo * sizeof(char) );
    for(int i = 0; i < t_viejo; i++)
    {
        nuevo[i] = viejo[i];
    }
    free(viejo);
    viejo = nuevo;
    return viejo;
}



void planetasConArreglos(){
    char planetas[3];

    planetas[0] = 'T';
    planetas[1] = 'S';
    planetas[2] = 'l';

    //////
    printf("Ptolomeo:\n");
    imprimirPlanetas(planetas, 3);

    planetas[0] = 'S';
    planetas[1] = 'T';
    planetas[2] = '\0';

    //////
    printf("Juan:\n");
    imprimirPlanetas(planetas, 3);

    
    

    ///////
    printf("Galileo:\n");
    char planetas_internos[5];
    planetas_internos[0] = 'S';
    planetas_internos[1] = 'm';
    planetas_internos[2] = 'V';
    planetas_internos[3] = 'T';
    planetas_internos[4] = 'M';
    imprimirPlanetas(planetas_internos, 5);



    //////
    printf("Kepler\n");
    char planetas_internos_exteriores[9];
    planetas_internos_exteriores[0] = 'S';
    planetas_internos_exteriores[1] = 'm';
    planetas_internos_exteriores[2] = 'V';
    planetas_internos_exteriores[3] = 'T';
    planetas_internos_exteriores[4] = 'M';
    planetas_internos_exteriores[5] = 'J';
    planetas_internos_exteriores[6] = 'S';
    planetas_internos_exteriores[7] = 'U';
    planetas_internos_exteriores[8] = 'N';
    
    imprimirPlanetas(planetas_internos_exteriores, 9);


    //////
    printf("Fermi\n");
    char planetas_internos_exteriores_yPluton[10];
    planetas_internos_exteriores_yPluton[0] = '$';
    planetas_internos_exteriores_yPluton[1] = 'm';
    planetas_internos_exteriores_yPluton[2] = 'V';
    planetas_internos_exteriores_yPluton[3] = 'T';
    planetas_internos_exteriores_yPluton[4] = 'M';
    planetas_internos_exteriores_yPluton[5] = 'J';
    planetas_internos_exteriores_yPluton[6] = 'S';
    planetas_internos_exteriores_yPluton[7] = 'U';
    planetas_internos_exteriores_yPluton[8] = 'N';
    planetas_internos_exteriores_yPluton[9] = 'p';
    imprimirPlanetas(planetas_internos_exteriores_yPluton, 10);


    //////
    printf("Tyson\n");
    char planetas_internos_exteriores_yPluton_sinPluton[9];
    planetas_internos_exteriores_yPluton_sinPluton[0] = '$';
    planetas_internos_exteriores_yPluton_sinPluton[1] = 'm';
    planetas_internos_exteriores_yPluton_sinPluton[2] = 'V';
    planetas_internos_exteriores_yPluton_sinPluton[3] = 'T';
    planetas_internos_exteriores_yPluton_sinPluton[4] = 'M';
    planetas_internos_exteriores_yPluton_sinPluton[5] = 'J';
    planetas_internos_exteriores_yPluton_sinPluton[6] = 'S';
    planetas_internos_exteriores_yPluton_sinPluton[7] = 'U';
    planetas_internos_exteriores_yPluton_sinPluton[8] = 'N';
    imprimirPlanetas(planetas_internos_exteriores_yPluton_sinPluton, 9);

    return;
}


void planetasConPunteros(){
    char* planetas = (char*) malloc(3*sizeof(char));

    ///////
    printf("el primero\n");
    planetas[0] = 'T';
    planetas[1] = 'S';
    planetas[2] = 'l';

    imprimirPlanetas(planetas, 3);

    ///////
    printf("el segundo\n");
    planetas[0] = 'S';
    planetas[1] = 'T';
    planetas[2] = '\0';
    
    imprimirPlanetas(planetas, 3);

    planetas = estirar(planetas, 3, 5);

    printf("el tercero\n");
    planetas[0] = 'S';
    planetas[1] = 'm';
    planetas[2] = 'V';
    planetas[3] = 'T';
    planetas[4] = 'M';
    
    imprimirPlanetas(planetas, 5);

    planetas = estirar(planetas, 5,9);

    printf("el cuatro\n");
    planetas[0] = 'S';
    planetas[1] = 'm';
    planetas[2] = 'V';
    planetas[3] = 'T';
    planetas[4] = 'M';
    planetas[5] = 'J';
    planetas[6] = 'S';
    planetas[7] = 'U';
    planetas[8] = 'N';
    
    imprimirPlanetas(planetas, 9);


    planetas = estirar(planetas, 9, 10);
    printf("el quinto\n");
    planetas[9] = 'p';
   
    imprimirPlanetas(planetas, 10);

    free(planetas);
    return;
}



int main(int argc, char** argv)
{

    planetasConArreglos();

    printf("==============================\n");

    planetasConPunteros();


    return 0;
}