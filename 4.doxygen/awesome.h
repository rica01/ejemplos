/**
 * @file awesome.h
 *
 * @author Mary Shelly
 *
 * @version 0.5
 *
 * @copyright
 * Copyright (C) Parallax, Inc. 2012. All Rights MIT Licensed.
 *
 * @brief This library was created for the Library Studies tutorial, and
 * isn’t really all that useful otherwise.  It has functions for appending
 * what has been printed last with " is awesome!\n" and " is epic!\n" along
 * with some second counting features that run in another cog.  It’s main  
 * point is to provide example code for going step-by-step through creating
 * a Simple Library.

 */
#ifndef AWESOME_H                             // Prevents duplicate
#define AWESOME_H                             // forward declarations

#if defined(__cplusplus)                      // Keeps declarations
extern "C" {                                  // C++ compatible
#endif

#include "simpletools.h"                      // Requires simpletools

/**
 * @brief Append " is awesome!\n" to most recently printed text.  Also
 * append an internal count of number of times awesome was called.  This
 * count can be accessed by calling awesome_getCount.
 */
void awesome(void);                           // Forward declarations

/**
 * @brief Get the number of times awesome has been called.
 *
 * @returns number of times awesome has been called since the
 * application started.
 */
int  awesome_getCount(void);

/**
 * @brief Set the number of times awesome has been called to some
 * arbitrary value.
 *
 * @details This function doesn’t actually exist.  It’s just here to show
 * how the (at)param and (at)detail formatters can be used.
 *
 * @param countVal is the new value of number of times awesome has been called.
 *
 * @returns number of times awesome has been called before it was updated by
 * this function.
 */
int  awesome_setCount(int countVal);

/**
 * @brief Start a second timer in another cog.  This timer can be used
 * to get the time since the cog started.
 *
 * @returns 1 + the number of the cog that the process was launched into.
 */
int  awesome_startTimer(void);

/**
 * @brief Stop the second timer and reclaim the cog for other uses.
 */
void awesome_stopTimer(void);

/**
 * @brief Get seconds since the timer was started.
 *
 * @returns seconds measurement.
 */
int  awesome_secondsSince(void);

/**
 * @brief Reset the seconds count.
 */
int  awesome_secondsReset(void);

/**
 * @brief Append " is epic!\n" to most recently printed text.
 */
void epic(void);

#if defined(__cplusplus)                      // End C++ compatible block
}
#endif
/* __cplusplus */

#endif                                        // End prevent duplicate forward
/* AWESOME_H */                               // declarations block

/**
 * TERMS OF USE: MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */
