public class Avion {
    
    public String tipo;
    public String aerolinea;
    public int numPasajeros;
    public String puerto;
    
    Avion() {
        this.tipo = "jet";
        this.aerolinea = "LACSA";
        this.numPasajeros = 200;
        this.puerto = "CRC";
        return;
    }

    public void volar(String destino) {
        System.out.println("volando a " +destino);
        this.puerto=destino;
        return;
    }
}