#include <iostream>

using namespace std;

class Avion
{
public:
    string tipo;
    string aerolinea;
    int numPasajeros;
    string puerto;

    Avion();
    ~Avion();

    void volar(string destino);
};