#include "Avion.hpp"

Avion::Avion()
{
    cout << "construyendo... " << this << endl;
    this->tipo = "jet";
    this->aerolinea = "LACSA";
    this->numPasajeros = 200;
    this->puerto = "CRC";
    return;
}

Avion::~Avion()
{
    cout << "destruyendo... " << this << endl;
}

void Avion::volar(string destino)
{
    cout << "volando a " << destino << endl;
    this->puerto = destino;
    return;
}