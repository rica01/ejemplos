#include "Avion.hpp"

int main(int argc, char **argv)
{

    Avion dusty0;
    cout << "dusty0:" << endl;
    cout << "\taerolinea: " << dusty0.aerolinea << endl;
    cout << "\ttipo: " << dusty0.tipo << endl;
    cout << "\tcapacidad: " << dusty0.numPasajeros << endl;
    cout << "\tpuerto: " << dusty0.puerto << endl;

    dusty0.volar("Borneo");
    cout << "\tpuerto: " << dusty0.puerto << endl << endl;;


    Avion dusty1 = Avion();
    cout << "dusty1:" << endl;
    cout << "\taerolinea: " << dusty1.aerolinea << endl;
    cout << "\ttipo: " << dusty1.tipo << endl;
    cout << "\tcapacidad: " << dusty1.numPasajeros << endl;
    cout << "\tpuerto: " << dusty1.puerto << endl;

    dusty1.volar("Bahamas");
    cout << "\tpuerto: " << dusty1.puerto << endl << endl;


    Avion *dusty2 = new Avion();
    cout << "dusty2:" << endl;
    cout << "\taerolinea: " << dusty2->aerolinea << endl;
    cout << "\ttipo: " << dusty2->tipo << endl;
    cout << "\tcapacidad: " << dusty2->numPasajeros << endl;
    cout << "\tpuerto: " << dusty2->puerto << endl;

    dusty2->volar("Belize");
    cout << "\tpuerto: " << dusty2->puerto << endl << endl;;


    delete dusty2;
    return 0;
}