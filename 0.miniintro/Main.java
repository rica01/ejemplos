public class Main {
    public static void main(String args[]) {


        Avion dusty0 = new Avion();
        System.out.println("dusty0:");
        System.out.println("\taerolinea: "+dusty0.aerolinea);
        System.out.println("\ttipo: "+dusty0.tipo);
        System.out.println("\tcapacidad: "+dusty0.numPasajeros);
        System.out.println("\tpuerto: "+dusty0.puerto);

        dusty0.volar("Borneo");
        System.out.println("\tpuerto: "+dusty0.puerto);
        return;
    }
}