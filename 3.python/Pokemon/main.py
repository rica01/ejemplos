
from .Tipos.ElectricType import ElectricTypeClass
from .Tipos.FlyingType import FlyingTypeClass

from .Pokemon import PokemonClass

from .Pokemones.Zapdos import ZapdosClass

print("========================")
machamp = PokemonClass()
print(machamp.HP)
machamp.HP = 999999
print(machamp.HP)
machamp.metodo0()
machamp.__metodo1__()
print(machamp)
print("========================")
ratata = PokemonClass("pablo", "F", 100, 1, 1)
print(ratata)

print("========================")
sparrow = FlyingTypeClass()
print(sparrow)
print(sparrow.tipo)

print("========================")
zapdosPepe = ZapdosClass("pepe", "N/A", 100, 100, 0)
print(zapdosPepe)
