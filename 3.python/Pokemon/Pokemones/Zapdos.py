from ..Tipos.FlyingType import FlyingTypeClass
from ..Tipos.ElectricType import ElectricTypeClass


class ZapdosClass(FlyingTypeClass, ElectricTypeClass):
    def __init__(self, n="", s="", l=0, hp=0, nE=0):
        FlyingTypeClass.__init__(self, n, s, l, hp, nE)
        ElectricTypeClass.__init__(self, n, s, l, hp, nE)
        self.atk0 = "asdf"
        self.atk1 = "zxcv"
        self.atk2 = "qwer"
        self.atk3 = ""

    def __str__(self):
        s = FlyingTypeClass.__str__(self)
        s += "\n\t"+self.tipo
        return s
