

class PokemonClass:

    def __init__(self, n="", s="", hp=0, l=0, nE=0):
        self.nombre = n
        self.sexo = s
        self.HP = hp
        self.lvl = l
        self.numEvoluciones = nE

    def __metodo1__(self):
        print("soy un metodo \"privado\"")
        return

    def metodo0(self):
        print("soy un metodo publico")
        return

    def __str__(self):
        s = self.nombre + ": \n\t" + self.sexo + "\n\t" + str(self.lvl)
        return s
