from ..Pokemon import PokemonClass


class FlyingTypeClass(PokemonClass):
    def __init__(self, n="", s="", l=0, hp=0, nE=0):
        PokemonClass.__init__(self, n, s, l, hp, nE)
        self.tipo = "Volador"

    def __str__(self):
        s = PokemonClass.__str__(self)
        s += "\n\t"+self.tipo
        return s
