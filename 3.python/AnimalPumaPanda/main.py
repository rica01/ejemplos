import Panda
import Panda as P
from Panda import cc

import PumaPumoides

from PumaPumoides import PumaPumoides
from Felidae import Felidae

from Animal import Animal


def basicos():
    linea = "termina en "

    if True != False:
        i = 3
        print("diferentes")
    else:
        print("error")

    print(Panda.nombrePanda)
    print(P.nombrePanda)

    print(cc)


    Panda.imprimirPanda()
    Panda.pesoPandaPlaneta()

    Panda.pesoPandaPlaneta(27.4, "Zeus")
    Panda.pesoPandaPlaneta(planeta="Hades")


    lista0 = list()
    lista0 = []
    lista0 = [1,2,4]

    print(lista0[0])
    print(lista0[1])
    lista0[2] = 420
    print(lista0[2]) 

    print("===================")
    # for ALGO in COSA:
    for item in lista0:
        print(item)
    print("===================")
    unEstrin = "Saprissa pierde hoy"
    for letra in unEstrin:
        print(letra)
    print("===================")

    for i in range(2, -1, -1):
        item = lista0[i]
        print(item)

    print("===================")

    # #while CONDICION:
    # while 1 < 2:
    #     print("hola")

    opcion = int(input("poner opcion"))

    if opcion == 0:
        print("a")
    elif opcion == 1:
        print("b")
    elif opcion == 2:
        print("c")
    else:
        print("error")
    return

def clases():
    bajira = PumaPumoides("Bajira", 40, 2.45, 536)
    bajira.imprimirPuma()

    print("============================")

    jlr = PumaPumoides("Jose Luis Rodriguez", 70, 1.60, 68734)
    jlr.imprimirPuma()

    print("============================")

    otroPuma = bajira * jlr
    otroPuma.imprimirPuma()

    ~jlr
    ~bajira
    ~otroPuma


    print(jlr.velocidad)
    jlr.sonar()

    jlr.animalear()
    return


#####################

# basicos()
clases()
























