from abc import *

class Animal(metaclass=ABCMeta):
    def __init__(self):
        self.i = 42
        return

    @abstractmethod
    def animalear(self):
        pass