

def factorial_recursivo(n):
    if n == 1 or n == 0:
        return 1
    else:
        return n * factorial_recursivo(n-1)


def factorial_iterativo(n):  # esta bueno!
    if n == 0 or n == 1:
        return 1
    else:
        x = 1
        for i in range(1, n+1):
            x = x*i
        return x
