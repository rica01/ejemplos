import funciones as F


print("ola k asen")

l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
x = 123
y = "ola"
z = 456.321
w = True
print("========================")
if x == 123:
    print("son iguales")
    x += 100
else:
    print("son diferentes")
    x -= 100
print("========================")
if x == 123:
    print("son iguales")
    x += 100
elif x == 1:
    print("algo")
elif x == 0:
    print("algo mas")
else:
    print("al fin")

print("========================")
while x > 0:
    print(x)
    x -= 50

print("========================")
for i in range(0, len(l)):  # la manera c de imprmir listas
    print(l[i])

print("========================")
for item in l:  # la manera pythonica de imprimir listas
    print(item)

print("========================")
fact5 = F.factorial_recursivo(5)
print(fact5)

fact5 = F.factorial_iterativo(5)
print(fact5)
