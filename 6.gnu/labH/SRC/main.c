/** \file main.c
 * Punto de entrada para el programa que hace ALGO BONITO.
 *
 * \author Ricardo Roman-Brenes <ricardo.roman@ucr.ac.cr>
 * \version 1.2
 * \date 2019
 */

#include <stdio.h> // para imprimir
#include <stdlib.h> // monton de funciones utiles
#include "../INCL/funciones.h"


/**
 * Funcion main, punto de entrada.
 *
 * Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.Funcion main, punto de entrada.
 *
 * @param argc Variable de tipo entero, cantidad de parametros envios por la CLI.
 * @param argv Variable de tipo puntero doble de caracteres, valores de los parametros envaidos por la.
 * @returns Codigo de salida del programa. 0 es Finalizacion exitoso.
 */
int main(int argc, char** argv) {

    // comentario de 1 linea
    /*
        comentario de varias lineas
    */
    printf("Hola, soy un programita que hace ALGO BUENO.\n");
    
    
    int a = calcular_genes(5); /// Valor a imprimir.

    printf("El resultado es: %d\n", a);

    return 0;
}

