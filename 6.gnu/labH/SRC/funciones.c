/** \file funciones.c
 * Implementacion de funciones declaradas en funciones.h
 *
 * \author Ricardo Roman-Brenes <ricardo.roman@ucr.ac.cr>
 * \version 1.2
 * \date 2019
 */

#include "../INCL/funciones.h"

int calcular_genes(int x) {

    int s = 4200*x; /// Variable de tipo entera. Guarda los genes. Si fuera HOMO ULTRA, necesita otro tipo de dato.
    return s;
}



