#ifndef BICHOVIVO_H
#define BICHOVIVO_H

// bibliotecas
#include <iostream>

using namespace std;

class BichoVivo {
    public:
        unsigned long id;
        string nombre;
        string nombreCientifico;
        string nombreComun;
        int edad;
        unsigned long long numCelulas;

        BichoVivo();
        BichoVivo(unsigned long, string, string, string, int, unsigned long long);
        ~BichoVivo();

        virtual void nacer();
        void crecer();
        virtual void reproducir() = 0;
        void morir();

    private:
    protected:
};

#endif







