#ifndef PEGASO_H
#define PEGASO_H

#include <iostream>

using namespace std;

class Pegaso {
    public:
        Pegaso();
        ~Pegaso();

        void nacer();
        void crecer();
        void reproducir();
        void morir();

        void volar();
};

#endif