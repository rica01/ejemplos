#ifndef MINOTAURO_H // code guard
#define MINOTAURO_H

#include <iostream>
#include "BichoVivo.h"
using namespace std;

class Minotauro : public BichoVivo {
    public:
        Minotauro();
        Minotauro(string s);
        ~Minotauro();


        bool operator!();
        Minotauro* operator*(Minotauro);
        void nacer();
        // void nacer();
        // void crecer();
        void reproducir();
        // void morir();

        void noPerderse();
};

#endif