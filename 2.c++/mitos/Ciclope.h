#ifndef CICLOPE_H
#define CICLOPE_H

#include <iostream>

using namespace std;

class Ciclope {
    public:
        Ciclope();
        ~Ciclope();

        void nacer();
        void crecer();
        void reproducir();
        void morir();

        void dispararRayos();
};

#endif