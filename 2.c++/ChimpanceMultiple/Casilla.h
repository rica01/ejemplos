#ifndef CASILLA_H
#define CASILLA_H
#include <iostream>
using namespace std;
class Casilla {


    public:
        virtual void imprimir() = 0;
        virtual string getNombre() = 0;
        virtual void setNombre(string s) = 0;

    protected:
        string nombre;



};
#endif