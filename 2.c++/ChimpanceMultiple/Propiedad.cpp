#include "Propiedad.h"


Propiedad::Propiedad() {

};
Propiedad::Propiedad(string n, uint p, uint r, char g, string d) {
        this->nombre = n; 
        this->precio = p; 
        this->renta = r; 
        this->grupo = g; 
        this->propietario = d; 

};

Propiedad::~Propiedad() {

};

void Propiedad::imprimir() { 
        cout << "\t\t" << this->nombre << " | precio: ";
        cout << this->precio << " | renta: ";
        cout << this->renta << " | grupo: ";
        cout << this->grupo << " | propietario: ";

        cout << this->propietario << endl; 
        return; 
};

string Propiedad::getNombre() {
        return this->nombre;
};

void Propiedad::setNombre(string s) {
        this->nombre = s;
};

uint Propiedad::getPrecio() {
        return this->precio;
};
void Propiedad::setPrecio(uint p) {
        this->precio = p;
};