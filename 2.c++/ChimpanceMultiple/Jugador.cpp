#include "Jugador.h"

Jugador::Jugador() {
    this->nombre = "jugador";
    this->ficha = 'X';
    this->plata = 1500;
    this->indiceCasilla = 0;
    cout << "creando  " << this->nombre << "@" << this << endl;
}

Jugador::Jugador(string n, unsigned int p, char f, unsigned int c) {
    this->nombre = n;
    this->plata = p;
    this->ficha = f;
    this->indiceCasilla = c;
    cout << "creando  " << this->nombre << "@" << this << endl;
}

Jugador::~Jugador() {
    cout << "destruyendo " << this->nombre << "@" << this << endl;
}

unsigned int Jugador::tirarDados() {
    unsigned int d1 = this->generarEntero(1, 6);
    unsigned int d2 = this->generarEntero(1, 6);
    return d1 + d2;
}

// bool Jugador::hipotecarPropiedad() {
//     return true;
// }


// bool Jugador::deshipotecarPropiedad() {
//     return true;
// }

int Jugador::generarEntero(int limInf, int limSup){
    random_device dev;
    mt19937 rng(dev());
    uniform_int_distribution<mt19937::result_type> gen(limInf, limSup); // distribution in range [1, 6]

    return gen(rng);
}