#ifndef PROPIEDAD_H
#define PROPIEDAD_H
#include <iostream>


#include "Casilla.h"

using namespace std;

class Propiedad : public Casilla {
    public:


        Propiedad();
        Propiedad(string n, uint p, uint r, char g, string d);
        ~Propiedad();

        void imprimir();

        string getNombre();
        void setNombre(string s);

        uint getPrecio();
        void setPrecio(uint p);

    private:
        uint precio;
        string propietario;
        bool hipotecada;
        uint renta;
        char grupo;
};
#endif