#ifndef JUGADOR_H
#define JUGADOR_H


#include <iostream>
using namespace std;
#include <random>

class Jugador {
    public: 
        Jugador();
        Jugador(string, unsigned int, char, unsigned int);
        ~Jugador();

        unsigned int tirarDados();
        

        void setNombre(string n) { this->nombre = n; return; };
        void setFicha(char f) {this->ficha = f; return; };
        void setPlata(uint p) {this->plata = p; return; };
        void setIndicePosicion(uint i) {this->indiceCasilla = i; return; };

        string getNombre() {return this->nombre;};
        char getFicha() {return this->ficha;};
        uint getPlata() {return this->plata;};
        uint getIndicePosicion() {return this->indiceCasilla;};
    private:
        string nombre;
        unsigned int plata;
        char ficha;
        uint indiceCasilla;
        
        
        int generarEntero(int limInf, int limSup);

};


#endif