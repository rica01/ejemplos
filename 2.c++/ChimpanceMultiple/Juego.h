#ifndef JUEGO_H
#define JUEGO_H

#include <iostream>
using namespace std;
#include "Jugador.h"
#include "Casilla.h"
#include "Propiedad.h"
#include "Tren.h"
#include "Mala.h"
#include "Juego.h"


class Juego {
    public:
        Juego();
        Juego(uint);
        ~Juego();
        
        Jugador* jugadores;
        Casilla** tablero;
        
        // CartaSuerte* suertes;
        // CartaArca* arcas;
        int s;
        int a;
        // ...
        
        void jugar();
        void imprimirTablero();
    private:
        Jugador* generarJugadores(uint);
        Casilla** generarTablero();

};




#endif