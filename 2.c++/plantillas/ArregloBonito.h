#include <iostream>
using namespace std;

class ArregloBonito {
    public:
        ArregloBonito(){ return; };
        ArregloBonito(uint i){ N = i; datos = new bool[N]; return; };
        ArregloBonito(const ArregloBonito &orig){ return; };
        ~ArregloBonito(){ return; };

        int get_n(){ return n; }
        int get_N(){ return N; }
        bool getDato(uint i) { return datos[i]; }
        void setDato(uint i, bool b) { datos[i] = b; return; }
        bool operator[](uint i) { return datos[i]; }

    private:
        int n;
        int N;
        bool* datos;
};