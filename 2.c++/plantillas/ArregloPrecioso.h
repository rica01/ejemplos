#include <iostream>

using namespace std;

template <typename TipoPrecioso>
class ArregloPrecioso {
    public:
        ArregloPrecioso(){};
        ArregloPrecioso(uint i){ N = i; datos = new TipoPrecioso[N]; return; };

        ~ArregloPrecioso(){};

        int get_N(){ return N; }
        TipoPrecioso getDato(uint i) { return datos[i]; }
        void setDato(uint i, TipoPrecioso b) { datos[i] = b; return; }
        TipoPrecioso operator[](uint i) { return datos[i]; }

        void imprimir() { for(uint i = 0; i<N; i++) cout << datos[i] << endl; }
    private:
        uint N;
        TipoPrecioso* datos;
};