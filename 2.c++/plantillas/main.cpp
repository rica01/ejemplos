#include <iostream>
#include <stdio.h>
#include "ArregloBonito.h"
#include "ArregloBonitoString.h"


#include "ArregloPrecioso.h"
using namespace std;

// void imprimir(bool miArreglito[7]){
//     for(uint i = 0; i < 7; i++)
//         cout << miArreglito[i] << endl;
//     return;
// }

// void imprimirEnC(bool* a, uint n){
//     for(uint i = 0; i<n; i++)
//         cout << a[i] << endl;
//     return;
// }

// void imprimirArregloBonito(ArregloBonito* ab) {
//     for(uint i = 0; i < ab->get_N(); i++)
//         printf("%d\n", (*ab)[i]);
//         // cout << (*ab)[i] << endl;
//     return;
// }

// void imprimirArregloBonitoString(ArregloBonitoString* ab) {
//     for(uint i = 0; i < ab->get_N(); i++)
//         // printf("%s\n", (*ab)[i]);
//         cout << (*ab)[i] << endl;
//     return;
// }


// template<string, typename ElOtroTipo>
// bool miMin(string a, ElOtroTipo b){
//     cout << "string, elotrotipo" << endl;
//     return 0;
// }

// template<typename ElTipo, string>
// bool miMin(ElTipo a, string b){
//     cout << "eltipo, string" << endl;
//     return 0;
// }

template<typename ElTipo, typename ElOtroTipo>
bool miMin(ElTipo a, ElOtroTipo b){
    cout << "caso general" << endl;
    return a < b;
}

int main()
{
    int a = 3;
    int b = 4;
    bool min = miMin(a, b);

    cout << min << endl;





    // bool miArreglito[3];
    // float f;
    // double d;

    // imprimir(miArreglito);

    // cout << "****************************" << endl;

    // ArregloBonito preg_8(8);
    // preg_8.setDato(0, true);
    // preg_8.setDato(1, true);
    // preg_8.setDato(2, true);
    // preg_8.setDato(3, true);
    // preg_8.setDato(4, true);
    // preg_8.setDato(5, true);
    // preg_8.setDato(6, true);
    // preg_8.setDato(7, true);
    // imprimirArregloBonito(&preg_8);

    // ArregloBonitoString preg_9(8);
    // preg_9.setDato(0, "true");
    // preg_9.setDato(1, "salvamontes");
    // preg_9.setDato(2, "barney");
    // preg_9.setDato(3, "firulais"); // +1
    // preg_9.setDato(4, "goke");
    // preg_9.setDato(5, "pikachu");
    // preg_9.setDato(6, "doggy");
    // preg_9.setDato(7, "alex");
    // imprimirArregloBonitoString(&preg_9);

    // ArregloPrecioso<bool> _1(3);
    // _1.setDato(0, false);
    // _1.setDato(1, false);
    // _1.setDato(2, false);
    // _1.imprimir();

    // ArregloPrecioso<string> _2(3); 
    // _2.setDato(0, "pato1");
    // _2.setDato(1, "pato2");
    // _2.setDato(2, "pato3"); 
    // _2.imprimir();

    // ArregloPrecioso<double> _3(3); 
    // _3.setDato(0, 3.14);
    // _3.setDato(1, 2.87);
    // _3.setDato(2, 1.61); 
    // _3.imprimir();

    return 0;
}
