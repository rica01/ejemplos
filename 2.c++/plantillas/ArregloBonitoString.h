#include <iostream>
using namespace std;

class ArregloBonitoString {
    public:
        ArregloBonitoString(){ return; };
        ArregloBonitoString(uint i){ N = i; datos = new string[N]; return; };
        ArregloBonitoString(const ArregloBonitoString &orig){ return; };
        ~ArregloBonitoString(){ return; };

        int get_n(){ return n; }
        int get_N(){ return N; }
        string getDato(uint i) { return datos[i]; }
        void setDato(uint i, string b) { datos[i] = b; return; }
        string operator[](uint i) { return datos[i]; }

    private:
        int n;
        int N;
        string* datos;
};