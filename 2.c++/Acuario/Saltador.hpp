#ifndef SALTADOR
#define SALTADOR

#include "Pez.hpp"
#include <iostream>

using namespace std;

class Saltador : public Pez {

    public:
        Saltador(){};
        ~Saltador(){};

        void nadar() { 
            cout << "soy un saltador que saltea" << endl;
        }
        void reproducir(Pez* p){};
};

#endif