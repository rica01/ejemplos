#ifndef ANGUILA
#define ANGUILA

#include "Pez.hpp"
#include <iostream>

using namespace std;

class Anguila : public Pez {

    public:
        Anguila(){};
        ~Anguila(){};

        void nadar() { 
            cout << "soy una anguila que anguilea" << endl;
        }
        void reproducir(Pez* p){};
};

#endif