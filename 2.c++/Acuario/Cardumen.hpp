#include <iostream>
#include "Pez.hpp"

using namespace std;
class Cardumen {
public:
    Pez individuosDentroDelCardumen[10];
    int cantidadPeces = 10;
    /*
    ...
    */

    Pez& operator[](uint i) { return individuosDentroDelCardumen[i]; };
};