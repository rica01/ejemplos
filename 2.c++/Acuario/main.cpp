#include <iostream>
// #include "Pez.hpp"
// #include "Cardumen.hpp"
#include "PagrusPagrus.hpp"
#include "TilapiineCichlid.hpp"
#include "SalvelinusProfundus.hpp"
#include "Saltador.hpp"
#include "Anguila.hpp"
#include "CaballitoDeMar.hpp"


// sobrecarga doble check azul
// herencia doble check
// plantillas // templates
using namespace std;


template<typename CualquierPez>
CualquierPez imprimirCosa(CualquierPez x)
{
    cout << x << endl;
    return x;
}



int main(int argc, char **argv)
{
    imprimirCosa(2);
    imprimirCosa(2.0f);
    imprimirCosa(2.2);
    imprimirCosa("asdjfhla");
    imprimirCosa('x');
    
    // int a;
    // a = 3 + 4;

    // int x = 10, y = 11;
    // a = x + y;

    // int *x_ptr;
    // int *y_ptr;
    // x_ptr = new int();
    // y_ptr = new int();
    // *x_ptr = 5;
    // *y_ptr = 7;
    // a = (*x_ptr) + (*y_ptr);

    // Pez marlin;
    // marlin.nombre = "Marlin";
    // Pez dory;
    // dory.nombre = "Dory";

    // marlin + dory;
    // marlin.casarseCon(dory);

    // Pez nemo;
    // nemo.nombre = "Nemo";

    // marlin + nemo;
    // marlin + 420;

    // Cardumen especiePremnasBiaculeatus;
    // especiePremnasBiaculeatus.individuosDentroDelCardumen[1];
    // especiePremnasBiaculeatus[1];

    // Pez p, q, r, s, t;
    // q.nombre = "q";
    // r = s = t = p = q; // op=
    // cout << s.nombre << endl;
    // Pez u = q; // copyctr // Pez u = Pez(q);
    // cout << u.nombre << endl;

    // cout << !u << endl;
    // class Fraccion
    // {
    // public:
    //     int n;
    //     int d;
    //     Fraccion(){};
    //     Fraccion(int u, int d)
    //     {
    //         this->n = u;
    //         this->d = d;
    //     };
    //     virtual ~Fraccion(){};

    //     Fraccion operator*(const Fraccion &rhs)
    //     {
    //         int x = this->n * rhs.n;
    //         int y = this->d * rhs.d;
    //         return Fraccion(x, y);
    //     };
    // };

    // Fraccion _1(2,3);
    // Fraccion _2(4,3);
    // Fraccion _3;
    // _3 = _1 * _2;
    // cout << _3.n << "/" << _3.d << endl;

    // PagrusPagrus julia;
    // PagrusPagrus julio;
    // julio.color = julia.color = "rojo";

    // julio.nacer();
    // julia.nacer();

    // TilapiineCichlid tina;
    // tina.nombre = "tina";
    // cout << tina.nombre << endl;

    // TilapiineCichlid *tina_ptr;
    // tina_ptr->nombre = "tina";
    // cout << tina_ptr->nombre << endl;

    // f(tina, *tina_ptr);
    // Pez unaTrucha = TilapiineCichlid();
    // Pez* unaTrucha_ptr = new TilapiineCichlid();

    Pez* bichos[3];
    bichos[0] = new CaballitoDeMar();
    bichos[1] = new Anguila();
    /// ...
    bichos[2] = new Saltador();

    // cout << !bichos[0] << endl << endl;
    // cout << !bichos[1] << endl << endl;
    // cout << !bichos[2] << endl << endl;

    for(int i = 0; i < 3; i++)
        bichos[i]->nadar();

    imprimirCosa(*bichos[0]);




    AnimalAcuatico<string> a0;
    AnimalAcuatico<char*> a1;
    






    return 0;
}
