#ifndef PEZ // code guards
#define PEZ
#include <iostream>
#include "AnimalActuatico.hpp"



using namespace std;
class Pez : public AnimalAcuatico<string>
{
public:
    string geohabitat;
    string metodoDePesca;

    string nombre;
    uint dimension;
    double peso;
    string color;
    bool sexo;
    int edad;

    int i;

    Pez();
    Pez(const Pez &orig);
    virtual ~Pez();

    virtual void nadar();
    void comer();
    // virtual void reproducir(Pez* p) = 0;

    void nacer() { this->edad = 0; }
    void morir();

    void casarseCon(const Pez &rhs);
    void operator+(const Pez &rhs);
    void operator+(const int i);

    Pez operator=(const Pez &rhs);

    string operator!();
    void operator^=(const double &d);

    friend ostream& operator<<(ostream& os, const Pez& dt);

};

#endif