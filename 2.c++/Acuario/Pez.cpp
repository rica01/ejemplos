#include "Pez.hpp"

Pez::Pez()
{
    this->nombre = "pez";
    this->color = "color pez";
    this->dimension = 0.0;
    this->geohabitat = "Planeta Tierra"; 
    return;
}

Pez::Pez(const Pez &orig)
{
    this->nombre = orig.nombre;
}

Pez::~Pez()
{
}

void Pez::casarseCon(const Pez &rhs)
{

    cout << "se casaron " << this->nombre << " y " << rhs.nombre << endl;
    return;
}

void Pez::operator+(const Pez &rhs)
{
    cout << this->i + rhs.i << endl;
    return;
}

void Pez::operator+(const int i)
{
    cout << this->i + i << endl;
    return;
}

Pez Pez::operator=(const Pez &rhs)
{
    this->nombre = rhs.nombre;
    this->color = rhs.color;
    // ...
    return *this;
}

void Pez::operator^=(const double &d)
{
    //cosa rara;
}

string Pez::operator!()
{
    string s;
    s += this->nombre + " ";
    s += this->color + " ";
    s += to_string(this->dimension) + " ";
    return s;
}

void Pez::nadar(){

    cout << "soy un pez que nada" << endl;
    return;

}

void Pez::comer() {
    cout << "comiendo cadaveres" << endl;
    return;
}


ostream& operator<<(ostream& os, const Pez& dt)
{
    os << dt.nombre;
    return os;
}
