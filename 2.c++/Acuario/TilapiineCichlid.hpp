
#ifndef TILAPIA
#define TILAPIA

#include <iostream>
#include "Pez.hpp"

using namespace std;

class TilapiineCichlid : public Pez
{

public:
    uint caracteristaUnica_1;
    uint caracteristaUnica_2;

    TilapiineCichlid()
    {
        this->edad = -1;
        this->nombre = "Tilapita";
        this->color = "color Tilapia";
    };
    virtual ~TilapiineCichlid() {}
    void reproducir(Pez* p){};
};
#endif