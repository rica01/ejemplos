#ifndef PARGO
#define PARGO

#include <iostream>
#include "Pez.hpp"

using namespace std;

class PagrusPagrus : public Pez
{

public:
    uint caracteristaUnica_1;
    uint caracteristaUnica_2;
    uint caracteristaUnica_3;

    PagrusPagrus()
    {
        this->edad = -1;
    };
    PagrusPagrus(float f) : Pez()
    {
        this->dimension = f;
    };
    virtual ~PagrusPagrus(){};
    void reproducir(Pez* p){};
};
#endif