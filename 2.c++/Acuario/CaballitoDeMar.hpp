#ifndef CABALLITODEMAR
#define CABALLITODEMAR

#include "Pez.hpp"
#include <iostream>

using namespace std;

class CaballitoDeMar : public Pez {

    public:
        CaballitoDeMar(){};
        ~CaballitoDeMar(){};

        void nadar() { 
            cout << "soy un caballito que caballea" << endl;
        }
        void reproducir(Pez* p){};
};

#endif