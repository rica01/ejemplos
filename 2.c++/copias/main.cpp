
#include <iostream>
#include "Gordo.h"



using namespace std;

int main()
{

    Gordo g0; //ctr vacio
    Gordo g1(10); //ctr sobrecargado
    Gordo* g2 = new Gordo(3); //ctr con new

    cout << "=======================" << endl;

    Gordo g5 = g1; //ctr por copia
    cout << ~g1 << endl;
    cout << ~g5 << endl;
    cout << "-------------------------" << endl;
    g5 = *g2;
    cout << ~g5 << endl;

    ///////////////
    // cout << ~g0 << endl;
    // cout << ~g1 << endl;
    // cout << ~(*g2) << endl;
    ///////////////
    cout << "=======================" << endl;

    delete g2;


    cout << "|#|#|#|#|#|#|#|#|#|#|#|#|#|#|#|#|" << endl;

    //encadenaniento
    Gordo a,b,c,d,e;
    Gordo f(2);

    a = b = c = d = e = f;

    cout << "a:" << ~a << endl;
    cout << "b:" <<~b << endl;
    cout << "c:" << ~c << endl;
    cout << "d:" << ~d << endl;
    cout << "e:" << ~e << endl;
    cout << "f:" << ~f << endl;











}