#include <iostream>
#include <sstream>
using namespace std;



class Gordo {

    public:
        Gordo() {
            cout << this << "\tconstructor vacio" << endl;
            n=0;
            g=0x0;
        };

        Gordo(uint s) {
            cout << this << "\tconstructor sobrecargado" << endl;
            this->n = s;
            this->g = new double[this->n];
            this->fill();
        };

        Gordo(const Gordo &orig) {
            cout << this << "\tconstructor por copia " << &orig << endl;
            this->n = orig.n;
            this->g = new double[this->n];

            for(uint i = 0; i < this->n; i++)
                this->g[i] = orig.g[i];
        };

        ~Gordo() {
            cout << this << "\tdestructor" << endl;
            delete g;
        };

        // a + b;
        // a.operator+(b);
        Gordo& operator+(const Gordo &a) {
            this
        }


        Gordo& operator=(const Gordo &rhs) {
            cout << this << "\top=\t" << &rhs << endl;
            this->n = rhs.n;
            this->g = new double[this->n];

            for(uint i = 0; i < this->n; i++)
                this->g[i] = rhs.g[i];
            return *this;
        };

        string operator~() {
            stringstream s("", ios_base::app | ios_base::out);
            for(uint i = 0; i < n; i++)
            {
                s << static_cast<void*>(g) << "[" << i << "]=" << to_string(g[i]);
                s << endl;
            }
            return s.str();
        };

    private:
        void fill(){
            for(uint i = 0; i < n; i++) 
                g[i] = 1.1 * i;
        }

        uint n;
        double* g;

};