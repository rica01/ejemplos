#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>

using namespace std;
class Robot
{
public:
    Robot()
    {
        b = new bool();
        *b = false;
        cout << "default constructor" << endl;
    };

    Robot(const Robot &r)
    {
        cout << "copy constructor" << endl;
        this->name = r.name;
        this->charge = r.charge;
        this->memory = r.memory;
        this->model = r.model;
        this->purpose = r.purpose;
        return;
    };

    Robot(string n, uint c, uint m, string x, string p)
    {
        cout << "overloaded constructor" << endl;
        this->name = n;
        this->charge = c;
        this->memory = m;
        this->model = x;
        this->purpose = p;
        return;
    };

    ~Robot(){
        delete b;

    };

    Robot(string filename)
    {
        ifstream fp(filename);

        string s[5];
        uint i = 0;
        while (getline(fp, s[i++]))
            ; // bonito

        this->name = s[0].substr(s[0].find_first_of(" ") + 1, s[0].size() - s[0].find_first_of(" ") + 1);
        this->charge = stoi(s[1].substr(s[1].find_first_of(" ") + 1, s[1].size() - s[1].find_first_of(" ") + 1));
        this->memory = stoi(s[2].substr(s[2].find_first_of(" ") + 1, s[2].size() - s[2].find_first_of(" ") + 1));
        this->model = s[3].substr(s[3].find_first_of(" ") + 1, s[3].size() - s[3].find_first_of(" ") + 1);
        this->purpose = s[4].substr(s[4].find_first_of(" ") + 1, s[4].size() - s[4].find_first_of(" ") + 1);

        return;
    }

    void printRobot()
    {
        cout << this->name << endl;
        cout << "\t" << this->charge << endl;
        cout << "\t" << this->model << endl;
        cout << "\t" << this->memory << endl;
        cout << "\t" << this->purpose << endl;
        return;
    }

    void selfdestruct(uint min)
    {
        for (uint i = 0; i < min; i++)
            cout << i << endl;
        cout << "|<aPuT!" << endl;
        return;
    }

    void selfdestruct(float min, string s)
    {

        cout << min << endl;
        cout << "|<aPuT!" << endl;
        return;
    }

    void printToFile()
    {
        ofstream fp(this->name + ".rob");
        fp << "name " << this->name << endl;
        fp << "charge " << this->charge << endl;
        fp << "memory " << this->memory << endl;
        fp << "model " << this->model << endl;
        fp << "purpose " << this->purpose << endl;
        return;
    }

    // private:
    bool *b;
    string name;
    uint charge;
    uint memory;
    string model;
    string purpose;
};
