#include <iostream>
#include <fstream>
#include <string> // c++ strings

#include "Robot.h"

using namespace std;

int main()
{

    char *texto_cpp = new char(10);

    Robot WallE;
    Robot BenderBendingRodriguez;
    Robot MetaBee;

    Robot OptimusPrime = Robot();
    Robot OrionPax = Robot("Orion Pax", 420, 3, "Pax v1", "factory worker");
    Robot NorionWarx = Robot(OrionPax);
    NorionWarx.name = "NorionWarx";

    WallE.printRobot();
    BenderBendingRodriguez.printRobot();
    MetaBee.printRobot();
    OptimusPrime.printRobot();
    OrionPax.printRobot();
    NorionWarx.printRobot();

    Robot &x = WallE;

    WallE.name = "WaaaaAAAAaa@@@@lleeee";
    x.purpose = "cleaningbot";


    BenderBendingRodriguez.selfdestruct( 4 );
    BenderBendingRodriguez.selfdestruct( 0.4, "ns" );


    delete texto_cpp;
    


    Robot Baymax = Robot("./baymax.rob");
    Baymax.printRobot();

    WallE.printToFile();

    return 0;
}
