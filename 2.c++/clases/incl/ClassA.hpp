#ifndef CLASSA
#define CLASSA

#include <iostream>
#include <sstream>
using namespace std;

class ClassA {

    public:
        ClassA();
        ClassA(const ClassA& orig);
        ~ClassA();

        int getAttrib0();
        float getAttrib1();
        bool getAttrib2();

        void setAttrib0(int);
        void setAttrib1(float);
        void setAttrib2(bool);
        
        string toString();

        void method0();
        void method1(double, int);
        char method2();


    private:
        unsigned int id;
        int attrib0;
        float attrib1;
        bool attrib2;

};



#endif