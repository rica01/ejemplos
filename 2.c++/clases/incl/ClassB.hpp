#ifndef CLASSB
#define CLASSB

#include <iostream>
#include <sstream>
using namespace std;

class ClassB {

    public:
        ClassB();
        ClassB(const ClassB& orig);
        ~ClassB();

        unsigned long getAttrib0();
        short getAttrib1();
        float* getAttrib2();

        void setAttrib0(unsigned long);
        void setAttrib1(short);
        void setAttrib2(float*);
        
        string toString();

        void method0();
        void method1(double, int);
        char method2();


    private:
        unsigned int id;
        unsigned long attrib0;
        short attrib1;
        float* attrib2;
        

};



#endif