#include <iostream>
#include "ClassA.hpp"
#include "ClassB.hpp"

int main(int argc, char **argv)
{

    ClassA objectA;

    objectA.setAttrib0(33);
    objectA.setAttrib1(3.68);
    objectA.setAttrib2(true);

    cout << objectA.getAttrib1() << endl;

    objectA.method1(33.22, 5);
    char c = objectA.method2();

    cout << c << endl;
    cout << objectA.toString() << endl;

    //////////////////

    ClassB *objectB_ptr;
    objectB_ptr = new ClassB();

    objectB_ptr->setAttrib0(3141251234123);
    objectB_ptr->setAttrib1(2);

    float f = 123.123;
    objectB_ptr->setAttrib2(&f);

    cout << objectB_ptr->getAttrib1() << endl;

    objectB_ptr->method1(33.22, 5);
    char d = objectB_ptr->method2();

    cout << d << endl;
    cout << objectB_ptr->toString() << endl;

    return 0;
}