#include "ClassB.hpp"

ClassB::ClassB()
{
    this->attrib0 = 0;
    this->attrib1 = 0;
    this->attrib2 = new float();
    *this->attrib2 = 0.0f;
}

ClassB::ClassB(const ClassB &orig)
{
    this->attrib0 = orig.attrib0;
    this->attrib1 = orig.attrib1;
    this->attrib2 = new float();
    *this->attrib2 = *orig.attrib2;
}

ClassB::~ClassB()
{
    // empty destructor
}

unsigned long ClassB::getAttrib0()
{
    return this->attrib0;
}

short ClassB::getAttrib1()
{
    return this->attrib1;
}

float* ClassB::getAttrib2()
{
    return this->attrib2;
}

void ClassB::setAttrib0(unsigned long ul)
{
    this->attrib0 = ul;
    return;
}

void ClassB::setAttrib1(short s)
{
    this->attrib1 = s;
    return;
}

void ClassB::setAttrib2(float* f_ptr)
{
    this->attrib2 = f_ptr;
    return;
}

string ClassB::toString()
{
    stringstream ss;
    ss << this << endl;
    ss << "\t" << this->attrib0 << endl;
    ss << "\t" << this->attrib1 << endl;
    ss << "\t" << this->attrib2 << endl;
    return ss.str();
}

void ClassB::method0()
{
    //empty
    return;
}

void ClassB::method1(double d, int i)
{
    if (this->attrib2 == 0x0)
    {
        this->attrib1 = d;
        this->attrib0 = i;
        this->attrib2 = new float();
    }
    return;
}

char ClassB::method2()
{
    return 'C';
}
