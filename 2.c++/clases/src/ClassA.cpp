#include "ClassA.hpp"

ClassA::ClassA()
{
    this->attrib0 = 0;
    this->attrib1 = 0.0f;
    this->attrib2 = false;
}

ClassA::ClassA(const ClassA &orig)
{
    this->attrib0 = orig.attrib0;
    this->attrib1 = orig.attrib1;
    this->attrib2 = orig.attrib2;
}

ClassA::~ClassA()
{
    // empty destructor
}

int ClassA::getAttrib0()
{
    return this->attrib0;
}

float ClassA::getAttrib1()
{
    return this->attrib1;
}

bool ClassA::getAttrib2()
{
    return this->attrib1;
}

void ClassA::setAttrib0(int i)
{
    this->attrib0 = i;
    return;
}

void ClassA::setAttrib1(float f)
{
    this->attrib1 = f;
    return;
}

void ClassA::setAttrib2(bool b)
{
    this->attrib2 = b;
    return;
}

string ClassA::toString()
{
    stringstream ss;
    ss << this << endl;
    ss << "\t" << this->attrib0 << endl;
    ss << "\t" << this->attrib1 << endl;
    ss << "\t" << this->attrib2 << endl;
    return ss.str();
}

void ClassA::method0()
{
    //empty
    return;
}

void ClassA::method1(double d, int i)
{
    if (this->attrib2)
    {
        this->attrib1 = d;
        this->attrib0 = i;
        this->attrib2 = !this->attrib2;
    }
    return;
}

char ClassA::method2()
{
    return 'R';
}
